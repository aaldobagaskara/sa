import csv
import logging
import random
import re
import sys
from math import exp
from typing import List, Tuple

import xlsxwriter

logger = logging.getLogger(__name__)


class Lot:
    jobs = ...  # type: List[LotJob]

    def __init__(self, code, molding_duration, job_duration):
        self.code = code
        self.molding_duration = molding_duration
        self.job_duration = job_duration
        self.jobs = []

        # Memory variables
        self.maximum_completion_lot_job = None

    def get_maximum_completion_lot_job(self):
        return self.maximum_completion_lot_job

    def add_lot_job(self, lot_job):
        """
        Add Lot Job to the end of the queue
        """
        self.jobs.append(lot_job)

    def get_tardiness(self):
        """
        Calculate tardiness from maximum completion time - molding duration of an Lot
        :return: tardiness, None if there is no Lot Job with maximum completion time
        """
        if self.maximum_completion_lot_job is None:
            return None
        result = self.maximum_completion_lot_job.get_completion_time() - self.molding_duration
        result = 0 if result < 0 else result
        return result

    def update_maximum_completion_lot_job(self, moved_lot_job):
        """
        Update maximum_completion_lot_job if the new moved_lot_job is much later completed than the last
        :return: True if there is an update
        """
        result = False
        if self.maximum_completion_lot_job is None:
            # If empty, just write
            self.maximum_completion_lot_job = moved_lot_job
            moved_lot_job.set_as_maximum_completion_lot_job()
            result = True
        else:
            # Rescan all Lot Job to determine the maximum completion Lot Job
            for job in self.jobs:
                if job.get_completion_time() > self.maximum_completion_lot_job.get_completion_time():
                    self.maximum_completion_lot_job = job
                    result = True
        return result

    def update_registry(self, moved_lot_job):
        self.update_maximum_completion_lot_job(moved_lot_job)


class LotJob:
    lot = ...  # type: Lot
    number = ...  # type: int
    has_setup_time = ...  # type: bool
    is_maximum_completion_lot_job = ...  # type: bool
    current_machine = ...  # type: Machine
    current_machine_index = ...  # type: int
    current_machine_finish_time = ...  # type: int
    current_machine_start_time = ...  # type: int
    current_machine_queue_index = ...  # type: int

    def __init__(self, lot, number):
        # Main variables
        self.lot = lot
        self.number = number
        # Memory variables
        self.is_maximum_completion_lot_job = False
        # Add this Lot Job to Lot
        self.lot.add_lot_job(self)
        self.has_setup_time = False

        # Memory Variables
        self.current_machine = None
        self.current_machine_index = None
        self.current_machine_queue_index = None
        self.current_machine_start_time = None
        self.current_machine_finish_time = None

    def __repr__(self):
        return "{},{}|{:5}|{:06d}-{:06d}".format(self.lot.code, self.number,
                                                 "SETUP" if self.has_setup_time else "NOSET",
                                                 self.current_machine_start_time, self.current_machine_finish_time)

    def __str__(self):
        return "{},{}|{:5}|{:06d}-{:06d}".format(self.lot.code, self.number,
                                                 "SETUP" if self.has_setup_time else "NOSET",
                                                 self.current_machine_start_time, self.current_machine_finish_time)

    def get_name(self):
        return "i{},{}".format(self.lot.code, self.number)

    def update_registry(self, current_machine, current_machine_index, current_machine_queue_index,
                        current_machine_start_time, current_machine_finish_time, with_setup_time=None):
        self.current_machine = current_machine
        self.current_machine_index = current_machine_index
        self.current_machine_queue_index = current_machine_queue_index
        self.current_machine_start_time = current_machine_start_time
        self.current_machine_finish_time = current_machine_finish_time
        if with_setup_time is not None:
            self.has_setup_time = with_setup_time

    def set_as_maximum_completion_lot_job(self):
        """
        Set this Lot Job to be its maximum completion Lot Job for its Lot
        :return:
        """
        self.is_maximum_completion_lot_job = True

    def get_start_time(self) -> int:
        """
        Get this Lot Job completion time on current machine
        :return:
        """
        return self.current_machine_start_time

    def get_completion_time(self) -> int:
        """
        Get this Lot Job completion time on current machine
        :return:
        """
        return self.current_machine_finish_time

    def get_molding_duration(self) -> int:
        """
        Get this Lot Job completion time on current machine
        :return:
        """
        return self.lot.molding_duration

    def get_job_duration(self) -> int:
        """
        Get this Lot Job completion time on current machine
        :return:
        """
        return self.lot.job_duration

    def get_current_machine_location(self) -> (int, int):
        """
        Returns this Lot machine location
        :return: (Machine index, Lot Job Queue index on Machine)
        """
        return self.current_machine_index, self.current_machine_queue_index


class Machine:
    lot_job_queue = ...  # type: List[LotJob]
    setup_time = 150  # Hardcoded 150

    def __init__(self, machine_index, machine_no=None):
        self.machine_no = machine_no if machine_no is not None else machine_index
        self.lot_job_queue = []

        # Memory Variables
        # self.machine_index = None  # Index of this machine on list of machines
        self.machine_index = machine_index  # Index of this machine on list of machines

    def __str__(self):
        return "Machine #{}".format(self.machine_no)

    def __repr__(self):
        return "{}".format(self.machine_no)

    def get_name(self):
        return "{}".format(self.machine_no)

    def get_queue(self):
        queue = [lot_job for lot_job in self.lot_job_queue]
        return queue

    def add_lot_job(self, lot_job, queue_index: int = None):
        """
        Add Lot Job to the end of the queue or to the queue_index
        """
        if queue_index is not None:
            # If there is already an Lot Job on machine queue
            if queue_index == 0:
                # If put on the beginning on the queue
                # Always has setup time on the beginning of the queue
                has_setup_time = True
                start_time = 0
            else:
                # If put on the middle of the queue or later
                # Get the start time from the previous Lot Job Lot on the queue
                previous_lot_job = self.lot_job_queue[queue_index - 1]  # type: LotJob
                start_time = previous_lot_job.get_completion_time()
                has_setup_time = previous_lot_job.lot is not lot_job.lot

            # Calculate finish time
            finish_time = start_time + lot_job.get_job_duration()
            if has_setup_time:
                finish_time += self.setup_time

            self.lot_job_queue.insert(queue_index, lot_job)

            # Update Lot Job registry
            lot_job.update_registry(self, self.machine_index, queue_index, start_time, finish_time, has_setup_time)

            # Update Lot registry
            lot_job.lot.update_registry(lot_job)

            # Adjust all Lot Job on its right
            i = queue_index + 1
            check_setup_time = True  # Check the setup time for the first Lot on the next one of the queue
            while i < len(self.lot_job_queue):
                lot_job_check = self.lot_job_queue[i]  # type: LotJob
                needs_setup = None
                if check_setup_time:
                    needs_setup = lot_job_check.lot is not lot_job.lot
                    check_setup_time = False
                start_time = finish_time
                finish_time = start_time + lot_job_check.get_job_duration()
                if needs_setup:
                    finish_time += self.setup_time
                self.lot_job_queue[i].update_registry(self, self.machine_index, i,
                                                      start_time, finish_time, needs_setup)
                # Update Lot registry
                lot_job.lot.update_registry(lot_job)
                i += 1

        else:
            # TODO Optimization; might be able to be joined with the upper if
            # End of the line
            if not self.lot_job_queue:
                # If the queue is empty
                # If put on the beginning on the queue
                # Always has setup time on the beginning of the queue
                has_setup_time = True
                start_time = 0
            else:
                # If the queue is not empty
                # Get the start time from the previous Lot Job Lot on the queue
                previous_lot_job = self.lot_job_queue[len(self.lot_job_queue) - 1]  # type: LotJob
                has_setup_time = previous_lot_job.lot is not lot_job.lot
                start_time = previous_lot_job.get_completion_time()

            # Calculate finish time
            finish_time = start_time + lot_job.get_job_duration()
            if has_setup_time:
                finish_time += self.setup_time

            self.lot_job_queue.append(lot_job)

            # Update Lot Job registry
            lot_job.update_registry(self, self.machine_index, len(self.lot_job_queue) - 1,
                                    start_time, finish_time, has_setup_time)

            # Update Lot registry
            lot_job.lot.update_registry(lot_job)

    def remove_lot_job(self, queue_index: int):
        """
        Remove selected Lot Job from queue by index
        """
        return self.lot_job_queue.pop(queue_index)


class Algorithm:
    machines = []  # type: List[Machine]
    lots = []  # type: List[Lot]

    def __init__(self, machines, lots, t, fp):
        self.machines = machines
        self.lots = lots
        # Waktu proses produksi
        # Tentukan parameter T, fp
        self.temperature = t
        self.fp = fp

    def compute_total_tardiness(self):
        """
        Calculate and update tardiness of all Lot and returns its total
        :return:
        """
        result = 0
        for lt in self.lots:
            lot_tardiness = lt.get_tardiness()
            if lot_tardiness is not None:
                result += lot_tardiness
        return result

    def _debug_print(self):
        for mach in self.machines:
            logger.debug("{}: {}".format(str(mach), mach.get_queue()))

    def run(self):
        self._debug_print()
        # Hitung tardiness(S) dari urutan job awal | current_tardiness == S
        tardiness = self.compute_total_tardiness()

        logger.info("INITIALIZATION ==================================================================================")
        logger.info("t             = {}".format(self.temperature))
        logger.info("fp            = {}".format(self.fp))
        logger.info("Tardiness     = {}".format(tardiness))
        logger.info("START ===========================================================================================")

        # Tetapkan iterasi i=1
        i = 1
        # Jumlah siklus p=1
        p = 1

        while not self.is_stopping_criteria_achieved():
            while not p >= 5:
                # -- Buat urutan job acak --
                i, p, tardiness = self._compute_cycle(i, p, tardiness)
                logger.info("k={:04d} Tardiness: {} | Temperature: {}".format(i, tardiness, self.temperature))
                # Apakah p>=5
            # Update siklus p=0
            p = 0
            # Kurangi temperatur T=Txfp
            new_temperature = self.temperature * self.fp
            logger.debug("Update Temperature ({}*{}) -> {}: ".format(self.temperature, self.fp, new_temperature))
            self.temperature = new_temperature
            logger.info("k={:04d} Tardiness: {} | Temperature: {}".format(i, tardiness, self.temperature))
            # Apakah stopping kriteria tercapai?

        # tardiness
        # Urutan Job
        # Waktu Komputasi
        logger.info("END =============================================================================================")
        logger.info("FINAL: Tardiness: {} | Temperature: {}".format(tardiness, self.temperature))
        logger.info("iteration (k): {}".format(i))
        self._debug_print()

    def _compute_cycle(self, i, p, tardiness):
        # -- Buat urutan job acak --
        # Membangkitkan 1 bilangan random untuk pengacakan
        r = random.uniform(0, 1)

        logger.debug(
            "+++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++")
        logger.debug("k = {}; p = {}".format(i, p))

        moved_lot_logs = None
        if r < 0.333:
            # Jika r<0,333
            logger.debug("Randomize FLIP (r < 0.333) ------------------------")
            # Pengacakan dengan cara flip
            moved_lot_logs = self.randomize_flip()
            logger.debug("---------------------------------------------------")
        elif 0.333 < r < 0.667:
            # Jika 0,333<r<0,667
            logger.debug("Randomize SWIPE (0.333 < r < 0.667) ---------------")
            # Pengacakan dengan cara swipe
            moved_lot_logs = self.randomize_swipe()
            logger.debug("---------------------------------------------------")
        elif r > 0.667:
            # Jika r>0,667
            logger.debug("Randomize SLIDE (r > 0.667)  ----------------------")
            # Pengacakan dengan cara slide
            moved_lot_logs = self.randomize_slide()
            logger.debug("---------------------------------------------------")

        # -- Urutan job baru --
        # Hitung tardiness(S')
        new_tardiness = self.compute_total_tardiness()

        # Hitung selisih tardiness(S''=S'-S)
        delta_tardiness = new_tardiness - tardiness
        logger.debug("T = {}; T' = {}; delta = {} ".format(tardiness, new_tardiness, delta_tardiness))

        # Hasil negatif
        # Terima solusi
        if not delta_tardiness < 0:
            # Gunakan kriteria metropolis
            logger.debug("Delta Tardiness > 0; Use Metropolis")
            metropolis_result = self._calculate_use_metropolis(delta_tardiness)
            if not metropolis_result:
                # Gunakan solusi awal
                # Revert move
                logger.debug("Revert to Previous Solution")
                for moved_lot_log in moved_lot_logs:
                    moved_lot_job_current_location = moved_lot_log[0].get_current_machine_location()
                    moved_lot_job_previous_location = moved_lot_log[1]
                    self._move_lot_job(moved_lot_job_current_location[0], moved_lot_job_current_location[1],
                                       moved_lot_job_previous_location[0], moved_lot_job_previous_location[1])
                new_tardiness = self.compute_total_tardiness()
                if new_tardiness != tardiness:
                    logger.warning("HEEYO new {} != old {}".format(new_tardiness, tardiness))

        # Solusi awal = Solusi baru
        tardiness = new_tardiness
        # Update iterasi i=i+1
        i = i + 1
        # Update siklus p=p+i
        p = p + 1
        return i, p, tardiness

    def is_stopping_criteria_achieved(self):
        """
        Stop if temperature <= 1
        :return:
        """
        result = False
        if self.temperature <= 1:
            result = True
        return result

    def _get_maximum_tardiness_lot_job(self):
        """
        Get Lot Job with the maximum tardiness of every lot
        :return:
        """
        max_tardiness_lot_job = None  # type: LotJob

        max_tardiness_value = None
        for available_lot in self.lots:
            if max_tardiness_value is None or max_tardiness_value < available_lot.get_tardiness():
                max_tardiness_lot_job = available_lot.get_maximum_completion_lot_job()
                max_tardiness_value = available_lot.get_tardiness()
        return max_tardiness_lot_job

    def _get_lot_job_from_location(self, location: Tuple[int, int]) -> LotJob:
        """
        Get Lot Job from location
        :param location: (machine index, machine lot job queue index)
        :return:
        """
        return self.machines[location[0]].lot_job_queue[location[1]]

    def randomize_lot_interchange(self) -> List[Tuple[LotJob, Tuple[int, int]]]:
        """
        Select one Lot with maximum tardiness; then swap with all Lot Job from random Lot
        :return: Tuple of moved Lot Job object and tuple of its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """
        moved_lot_job = self._get_maximum_tardiness_lot_job()  # type: LotJob
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        random_machine_index = random.randint(0, len(self.machines) - 1)
        random_lot_index = random.randint(0, len(self.lots) - 1)
        while random_machine_index == moved_lot_job_previous_location[0]:
            random_machine_index = random.randint(0, len(self.machines) - 1)
        random_lot_job_previous_location = (random_machine_index, random_lot_index)
        random_lot_job = self._get_lot_job_from_location(random_lot_job_previous_location)
        moved_lot = moved_lot_job.lot
        random_lot = random_lot_job.lot

        moved_lot_jobs_log = []
        move_index = 0
        for job in moved_lot.jobs:
            job_location = job.get_current_machine_location()
            moved_lot_jobs_log.insert(0, (job, job_location))
            logger.debug(
                "Move {} from {} to {}".format(job, job_location,
                                               random_lot_job_previous_location))
            self._move_lot_job(job_location[0], job_location[1],
                               random_lot_job_previous_location[0], random_lot_job_previous_location[1] + move_index)
            move_index += 1
        move_index = 0
        for job in random_lot.jobs:
            job_location = job.get_current_machine_location()
            moved_lot_jobs_log.insert(0, (job, job_location))
            logger.debug(
                "Move {} from {} to {}".format(job, job_location,
                                               moved_lot_job_previous_location))
            self._move_lot_job(job_location[0], job_location[1],
                               moved_lot_job_previous_location[0], moved_lot_job_previous_location[1] + move_index)
            move_index += 1
        return moved_lot_jobs_log

    def randomize_lot_insert(self) -> List[Tuple[LotJob, Tuple[int, int]]]:
        """
        Select one Lot with maximum tardiness; then move all LotJob with the same Lot to after the target random LotJob
        :return: Tuple of moved Lot Job object and tuple of its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """
        moved_lot_job = self._get_maximum_tardiness_lot_job()  # type: LotJob
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        random_machine_index = random.randint(0, len(self.machines) - 1)
        random_lot_index = random.randint(0, len(self.lots) - 1)
        while random_machine_index == moved_lot_job_previous_location[0]:
            random_machine_index = random.randint(0, len(self.machines) - 1)

        logger.debug("Move all LotJob of Lot {} on random target machine {} and lot index {}"
                     .format(str(moved_lot_job.lot),
                             str(self.machines[random_machine_index]),
                             str(random_lot_index)))

        # Move one by one
        source_machine = self.machines[moved_lot_job_previous_location[0]]  # type: Machine
        move_index = 1
        moved_lot_jobs_log = []
        for job in source_machine.lot_job_queue:
            job_location = job.get_current_machine_location()
            moved_lot_jobs_log.insert(0, (job, job_location))
            logger.debug(
                "Move {} from {} to {} at {}".format(moved_lot_job, moved_lot_job.get_current_machine_location(),
                                                     str(self.machines[random_machine_index]),
                                                     str(random_lot_index + move_index)))
            self._move_lot_job(job_location[0], job_location[1],
                               random_machine_index, random_lot_index + move_index)
            move_index += 1
        return moved_lot_jobs_log

    def randomize_job_interchange(self) -> List[Tuple[LotJob, Tuple[int, int]]]:
        """
        Select one Lot with maximum tardiness; then swap with random Lot Job
        :return: Tuple of moved Lot Job object and tuple of its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """
        moved_lot_job = self._get_maximum_tardiness_lot_job()  # type: LotJob
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        random_machine_index = random.randint(0, len(self.machines) - 1)
        random_lot_index = random.randint(0, len(self.lots) - 1)
        while random_machine_index == moved_lot_job_previous_location[0]:
            random_machine_index = random.randint(0, len(self.machines) - 1)

        logger.debug(
            "Swap on random target machine {} and lot index {}".format(str(self.machines[random_machine_index]),
                                                                       str(random_lot_index)))

        logger.debug("Swap {} from {} to {} at {}".format(moved_lot_job, moved_lot_job.get_current_machine_location(),
                                                          str(self.machines[random_machine_index]),
                                                          str(random_lot_index)))
        target_job_previous_location = (random_machine_index, random_lot_index)
        target_job = self._get_lot_job_from_location(target_job_previous_location)
        self._move_lot_job(moved_lot_job_previous_location[0], moved_lot_job_previous_location[1],
                           random_machine_index, random_lot_index)
        self._move_lot_job(random_machine_index, random_lot_index + 1,
                           moved_lot_job_previous_location[0], moved_lot_job_previous_location[1])
        moved_lot_jobs_log = [(moved_lot_job, moved_lot_job_previous_location),
                              (target_job,
                               target_job_previous_location)]
        return moved_lot_jobs_log

    def randomize_lot_merge(self) -> List[Tuple[LotJob, Tuple[int, int]]]:
        """
        Select one Lot with maximum tardiness; then place after a random Lot Job (on any machine) with the same Lot
        :return: Tuple of moved Lot Job object and tuple of its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """
        moved_lot_job = self._get_maximum_tardiness_lot_job()  # type: LotJob
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        random_lot_job_index = random.randint(0, len(moved_lot_job.lot.jobs) - 1)
        random_lot_job = moved_lot_job.lot.jobs[random_lot_job_index]  # type: LotJob

        target_machine_location = random_lot_job.get_current_machine_location()

        logger.debug("Move {} from {} to after {} on {}".format(moved_lot_job,
                                                                moved_lot_job.get_current_machine_location(),
                                                                str(random_lot_job),
                                                                target_machine_location))
        self._move_lot_job(moved_lot_job_previous_location[0], moved_lot_job_previous_location[1],
                           target_machine_location[0], target_machine_location[1] + 1)
        moved_lot_jobs_log = [(moved_lot_job, moved_lot_job_previous_location)]
        return moved_lot_jobs_log

    def randomize_item_insert(self) -> List[Tuple[LotJob, Tuple[int, int]]]:
        """
        Select one Lot with maximum tardiness; then place after a random Lot Job (on any machine) with the same Lot
        :return: Tuple of moved Lot Job object and tuple of its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """
        moved_lot_job = self._get_maximum_tardiness_lot_job()  # type: LotJob
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        random_lot_index = random.randint(0, len(self.lots) - 1)
        random_lot = self.lots[random_lot_index]  # type: Lot
        random_job_index = random.randint(0, len(random_lot.jobs) - 1)
        random_lot_job = random_lot.jobs[random_job_index]

        target_machine_location = random_lot_job.get_current_machine_location()

        logger.debug("Move {} from {} to after {} on {}".format(moved_lot_job,
                                                                moved_lot_job.get_current_machine_location(),
                                                                str(random_lot_job),
                                                                target_machine_location))
        self._move_lot_job(moved_lot_job_previous_location[0], moved_lot_job_previous_location[1],
                           target_machine_location[0], target_machine_location[1] + 1)
        moved_lot_jobs_log = [(moved_lot_job, moved_lot_job_previous_location)]
        return moved_lot_jobs_log

    def randomize_flip(self):
        """
        Select one code with maximum tardiness then place on random machine
        :return: Tuple of moved Lot Job object and tuple of its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """
        moved_lot_job = self._get_maximum_tardiness_lot_job()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        random_machine_index = random.randint(0, len(self.machines) - 1)
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        while random_machine_index == moved_lot_job_previous_location[0]:
            random_machine_index = random.randint(0, len(self.machines) - 1)
        logger.debug("Place on random target machine: {}".format(str(self.machines[random_machine_index])))

        logger.debug("Move FLIP {} from {} to {}".format(moved_lot_job, moved_lot_job_previous_location,
                                                         str(self.machines[random_machine_index])))
        self._move_lot_job(moved_lot_job_previous_location[0], moved_lot_job_previous_location[1],
                           random_machine_index)
        return [(moved_lot_job, moved_lot_job_previous_location)]

    def randomize_swipe(self):
        """
        Select one code with maximum tardiness then place on machine with minimum time
        :return: Tuple of moved Lot Job object and its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """

        def get_minimum_time_machine_index():
            result = None
            minimum_time = sys.maxsize
            for idx, mach in enumerate(self.machines):  # type: (int, Machine)
                machine_time = mach.lot_job_queue[
                    len(mach.lot_job_queue) - 1].current_machine_finish_time
                if machine_time < minimum_time:
                    minimum_time = machine_time
                    result = idx
            return result, minimum_time

        moved_lot_job = self._get_maximum_tardiness_lot_job()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        minimum_time_machine_index, minimum_time_machine_time = get_minimum_time_machine_index()
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        logger.debug(
            "Place on machine with lowest minimum time: {} of {}".format(str(self.machines[minimum_time_machine_index]),
                                                                         minimum_time_machine_time))

        logger.debug("Move SWIPE {} from {} to {}".format(moved_lot_job, moved_lot_job_previous_location,
                                                          str(self.machines[minimum_time_machine_index])))
        self._move_lot_job(moved_lot_job_previous_location[0], moved_lot_job_previous_location[1],
                           minimum_time_machine_index)
        return [(moved_lot_job, moved_lot_job_previous_location)]

    def randomize_slide(self):
        """
        Select one code with maximum tardiness then place on machine with the same code in it
        :return: Tuple of moved Lot Job object and its previous machine location
        (Machine index, Lot Job Queue index on machine)
        """

        def get_machine_with_same_lot_code_index(mvd_lot_job):
            result = None
            for idx, mach in enumerate(self.machines):  # type: (int, Machine)
                for lot_job in mach.lot_job_queue:  # type: LotJob
                    if lot_job.lot is mvd_lot_job.lot:
                        result = idx
                        if lot_job.current_machine_index != result:
                            break
            return result

        moved_lot_job = self._get_maximum_tardiness_lot_job()
        logger.debug("Maximum Tardiness Lot: {}".format(str(moved_lot_job)))

        same_lot_code_machine_index = get_machine_with_same_lot_code_index(moved_lot_job)
        moved_lot_job_previous_location = moved_lot_job.get_current_machine_location()
        logger.debug(
            "Place on machine with same code: {}".format(str(self.machines[same_lot_code_machine_index])))

        logger.debug("Move SLIDE {} from {} to {}".format(moved_lot_job, moved_lot_job.get_current_machine_location(),
                                                          str(self.machines[same_lot_code_machine_index])))
        self._move_lot_job(moved_lot_job_previous_location[0], moved_lot_job_previous_location[1],
                           same_lot_code_machine_index)
        return [(moved_lot_job, moved_lot_job_previous_location)]

    def _calculate_use_metropolis(self, delta_tardiness):
        r = random.uniform(0, 1)
        logger.debug("Metropolis: r = {}".format(r))
        exponential = exp(-delta_tardiness / self.temperature)
        result = r < exponential
        logger.debug(
            "Metropolis: r = {} --> ({}): {} < e^(-{}/{})".format(r, result, r, delta_tardiness, self.temperature))
        return result

    def _move_lot_job(self, source_machine_index, source_machine_queue_index, target_machine_index,
                      target_machine_queue_index=None):
        """
        Move Lot Job from its current machine to target_machine
        :param source_machine_index: The source machine index on machines variable
        :param source_machine_queue_index: The source machine queue index
        :param target_machine_index: The target machine index on machines variable
        :param target_machine_queue_index: The target machine queue index; None for last
        :return:
        """
        source_machine = self.machines[source_machine_index]  # type: Machine
        target_machine = self.machines[target_machine_index]  # type: Machine
        # Remove from current machine
        lot_job = source_machine.remove_lot_job(source_machine_queue_index)  # type: LotJob
        logger.debug("Move {} from {} to {}".format(lot_job, str(source_machine), str(target_machine)))

        # Adjust all Lot Job on its previous right
        i = source_machine_queue_index
        finish_time = source_machine.lot_job_queue[i - 1].current_machine_finish_time
        check_setup_time = True  # Check the setup time for the first Lot on the next one of the queue
        while i < len(source_machine.lot_job_queue):
            lot_job_check = source_machine.lot_job_queue[i]  # type: LotJob
            needs_setup = None
            if check_setup_time:
                needs_setup = lot_job_check.lot is not lot_job.lot
                check_setup_time = False
            start_time = finish_time
            finish_time = start_time + lot_job_check.get_job_duration()
            if needs_setup:
                finish_time += source_machine.setup_time
            source_machine.lot_job_queue[i].update_registry(source_machine, source_machine.machine_index, i,
                                                            start_time, finish_time, needs_setup)
            # Update Lot registry
            lot_job.lot.update_registry(lot_job)
            i += 1

        # Place on new machine
        if target_machine_queue_index is None:
            target_machine.add_lot_job(lot_job)
        else:
            target_machine.add_lot_job(lot_job, target_machine_queue_index)


class SAParser:
    machines = ...  # type: [Machine]
    lots = ...  # type: [Lot]

    def __init__(self):
        self.lots = []
        self.machines = []

    def read_input(self, gantt_csv_file, lot_data_csv_file):
        self._parse_input_lot_data(lot_data_csv_file)
        self._parse_input_gantt(gantt_csv_file)
        return self.machines, self.lots

    def _parse_input_gantt(self, gantt_csv_file):
        with open(gantt_csv_file) as csv_file:
            reader = csv.reader(csv_file, delimiter=';')
            machine_index = 0
            for row in reader:
                mach = Machine(machine_index)
                self.machines.append(mach)
                for cell in row:
                    if not cell:
                        continue
                    lot_code, number = self._read_gantt_cell(cell)
                    lot_code = lot_code.upper()
                    number = int(number)
                    lt = self._find_lot_from_lot_code(lot_code)
                    if lt is None:
                        raise Exception("Lot code {} is not registered for {}".format(lot_code, cell))
                    lot_job = LotJob(lt, number)
                    mach.add_lot_job(lot_job)
                machine_index += 1

    def _parse_input_lot_data(self, lot_data_csv_file):
        with open(lot_data_csv_file) as csv_file:
            reader = csv.reader(csv_file, delimiter=';')
            skip_first_row = True
            for row in reader:
                if skip_first_row:
                    skip_first_row = False
                    continue
                lot_name = row[0].upper()
                lot_molding = int(row[1])
                lot_job = int(row[2])
                lt = Lot(lot_name, lot_molding, lot_job)
                self.lots.append(lt)

    def write_gantt(self, gantt_output_file_name):
        with open(gantt_output_file_name, mode='w') as gantt_output_file:
            gantt_writer = csv.writer(gantt_output_file, delimiter=',')
            for mach in self.machines:  # type: Machine
                lot_jobs = [lot_job.get_name() for lot_job in mach.lot_job_queue]
                gantt_writer.writerow(lot_jobs)

    def write_gantt_xlsx(self, gantt_output_file_name):
        workbook = xlsxwriter.Workbook(gantt_output_file_name)
        worksheet = workbook.add_worksheet()

        row = 0
        col = 0
        for mach in self.machines:  # type: Machine
            worksheet.write(row, col, "M{}".format(mach.machine_no))
            col += 1
            for lot_job in mach.lot_job_queue:  # type: LotJob
                name = lot_job.get_name()
                time = "{}".format(lot_job.get_job_duration())
                if lot_job.has_setup_time:
                    name = "SETUP " + name
                    time = "SETUP 150 + " + time
                worksheet.write(row, col, name)
                worksheet.write(row + 1, col, time)
                col += 1
            row += 3
            col = 0

        workbook.close()

    def _find_lot_from_lot_code(self, lot_code):
        for lt in self.lots:
            if lt.code == lot_code:
                return lt
        return None

    def _read_gantt_cell(self, string):
        filter_result = re.match(r"([^\n]+),([^\n]+)", string, re.I)
        # filter_result = re.match(r"([a-z]+)([0-9]+)", string, re.I)
        if filter_result:
            lot_job = filter_result.groups()
            if len(lot_job) != 2:
                raise Exception("Invalid lot {}".format(string))
        else:
            return None, None
        return lot_job[0], lot_job[1]

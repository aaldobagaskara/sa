import argparse
import logging
import os
import sys
from datetime import datetime

from algorithm.algorithm import Lot, Machine, LotJob, SAParser, Algorithm


def debug_temp_item_machines():
    itemA = Lot('A', 1200, 100)
    itemB = Lot('B', 2400, 1000)
    items = [itemA, itemB]

    machine1 = Machine(0)
    for i in range(5):
        item_entity = LotJob(itemA, i + 1)
        machine1.add_lot_job(item_entity)
    for i in range(3):
        item_entity = LotJob(itemB, i + 1)
        machine1.add_lot_job(item_entity)
    machine2 = Machine(1)
    for i in range(5, 8):
        item_entity = LotJob(itemA, i + 1)
        machine2.add_lot_job(item_entity)
    for i in range(3, 5):
        item_entity = LotJob(itemB, i + 1)
        machine2.add_lot_job(item_entity)
    machine3 = Machine(2)
    for i in range(8, 10):
        item_entity = LotJob(itemA, i + 1)
        machine3.add_lot_job(item_entity)
    for i in range(5, 15):
        item_entity = LotJob(itemB, i + 1)
        machine3.add_lot_job(item_entity)

    machines = [machine1, machine2, machine3]
    return machines, items


def setup_logging(output_path, use_debug):
    logging_level = logging.DEBUG if use_debug else logging.INFO
    # logging.basicConfig(level=logging_level, filename='{}algorithm_flow.log'.format(output_path), filemode="a+",
    #                     format="%(asctime)-15s %(levelname)-8s %(message)s")
    logging.basicConfig(level=logging_level)


def argv_check(argv):
    try:
        if len(argv) != 2:
            raise Exception()
        arg_dict = {
            't': float(argv[0]),
            'fp': float(argv[1])
        }
    except:
        print('main.py <t> <fp>')
        sys.exit(2)
    return arg_dict


def arg_parsing():
    parser = argparse.ArgumentParser(description='')
    parser.add_argument('temperature', metavar='t', type=int, help='temperature')
    parser.add_argument('fp', metavar='fp', type=float, help='fp')
    parser.add_argument('-d', '--debug', action='store_true', help='Adds debug log on output')
    return parser.parse_args()


def main():
    args = arg_parsing()
    output_path = datetime.now().strftime("output/%Y-%m-%dT%H%M%S/")
    os.makedirs(output_path, exist_ok=True)
    setup_logging(output_path, args.debug)

    # TEST items and machines
    # machines, items = debug_temp_item_machines()

    parser = SAParser()
    machines, items = parser.read_input("input/gantt.csv", "input/item.csv")
    t = args.temperature
    fp = args.fp
    alg = Algorithm(machines, items, t, fp)
    alg.run()
    parser.write_gantt("{}out_gantt.csv".format(output_path))
    parser.write_gantt_xlsx("{}out_gantt.xlsx".format(output_path))


if __name__ == '__main__':
    main()
